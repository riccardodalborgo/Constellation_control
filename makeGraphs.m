%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   GRAPHS ROUTINE                                                        %
%   This routine computes all the graphs used in the report               %
%                                                                         %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all

addpath ('./data');
addpath ('./tools');
addpath ('./tools/animation');
addpath ('./tools/bellmanFord');
addpath ('./tools/conflicts');
addpath ('./tools/ctrl_design');
addpath ('./tools/network');
addpath ('./tools/SNR');
addpath ('./models');
addpath ('./tools/graphs');
addpath ('./simulation_results');

%% Color theme 
% blue = [0.25,0.58,0.95];
% green = [0.21, 0.72, 0.01];
% orange = [1, 0.5, 0];

%% NOTE:
% Sections "MCC ratio plot", "MCC curve fit" and "Weight results with N varying"
% works better with an higher number of mat-files in the folder linked by 'pth'.

%% MCC ratio plot
pth='simulation_results/network/';
MCCratio(pth)

%% MCC curve fit
pth='simulation_results/network/';
MCCcurveEstimate(pth)

%% Weight results with N varying
pth='simulation_results/network/';
weightEvolution(pth)

%% Alignement graphs
file ='simulation_results/graph50.mat';
alignmentPhases(file)

%% Bar Performance
file='simulation_results/network/graph50.mat';
performanceBar(file)

%% Cluster plot
file='simulation_results/graph50.mat';
clusterPlot(file)

%% Tree
file='./simulation_results/graph50.mat';
index=4;
communicationTrees(file, index)