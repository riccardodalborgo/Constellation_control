# Constellation Control

Group 4 Control System Design final project: proposal of a decentralized solution to the formation control of a CubeSat constellation, behaving as a distributed sensor network.  
The project is articulated in two sections:

* Implementation of a decentralized solution for the configuration of a large satellite formation based on the quality of the inter-satellite communications through an optimization technique.
* Analysis of the formed satellite network exploiting the Bellman-Ford algorithm to perform information routing among the agents.

## Getting Started
### Structure
The main file to launch the simulations is `main_sim.m`, which needs to be edited by the user to specify the desired simulation parameters and the graph. The performances of the network formed during the simulations can then be analyzed through `network_analysis.m`, and the result of the simulations can then be visualized using `makeGraphs.m`.

#### data/
Contains graphs on which the simulations are executed. Graphs are generated with the [Generate Graph](https://it.mathworks.com/matlabcentral/fileexchange/4768-generate-graphs) tool.

#### models/
Contains the Simulink scheme modelling the system.

#### simulation_results/
Stores the simulation results.

#### tools/
Contains all the functions used in the project. Organized in subfolders separating different fields. More informations can be found on the **/tool** subfolder README.

## Prerequisites

* MATLAB & Simulink software (recommended 2016b or later)
* [Generate Graph](https://it.mathworks.com/matlabcentral/fileexchange/4768-generate-graphs) tool for graph generation.

#### Usage of Generate Graph
In the downloaded folder:
 ```
 p = pwd;
 updatePath(p);
 G = makeGraph
 ```
 A GUI will open, specify the desired parameters and compute the graph properties, then
 ```
 graph = guidata(G.main_window);
 graph = graph.graph;
 save('<filename>.m', 'graph');
 ```



## Running the tests

Fistsly edit `main_sim.m` to specify
* desired simulation parameters
* _filename_ of the graph on which to perform the simulations

Then on MATLAB:
```
run main_sim.m
```

The simulation results are then stored in **/simulation_results** in the format
```
filename_<nsim>.mat
```
with _nsim_ simulation number, and _filename_ specified in `main_sim.m`.

The formed network can then be analyzed by
```
run network_analysis.m
```

The results are then stored in **simulation_results/network/** with the same format of the simulation results.

At this point, edit `makeGraphs.m` to specify the paths to the specific results required by sections _Alignement graphs_, _Bar Performance_, _Cluster plot_, _Tree_.  

The evolution of the system, and the network analysis results can finally be visualized running
```
run makeGraphs
```



## Authors

* **Riccardo Dal Borgo**: ID: 1153676, Email: <ricccardo.dalborgo@studenti.unipd.it>
* **Alberto Terzariol**: ID: 1159840, Email: <alberto.terzariol@studenti.unipd.it>
* **Alessandra Zampieri**: ID: 1153714, Email: <alessandra.zampieri.5@studenti.unipd.it>
* **Fabio Zorzetto**: ID: 1153701, Email: <fabio.zorzetto.1@studenti.unipd.it>




![image](https://i.pinimg.com/originals/b9/99/56/b999562fbed3038844f308228f2ece13.jpg)

