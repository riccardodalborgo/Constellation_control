function conflicts = checkConflicts(SNR_map,cubesat_type)
%conflicts = checkConflicts(SNR_map,cubesat_type)
%
% Checks the presence of conflicting communications.
%
% Inputs:
%
%   - SNR_map: N-by-N struct of 2-by-2 matrices of SNR values
%   - cubesat_type: 1-by-N vector of types 1/2/3 U
%
% Output:
%
%   - conflicts: 2-by-N vector of boolean flags
%

n = length(SNR_map);
%% check if conflict is present

a_is_conflicting = zeros(1,n);
b_is_conflicting = zeros(1,n);

for j = 1:length(SNR_map)
    count_a = 0;
    count_b = 0;
    
    for i = 1:length(SNR_map)
        % counting how many readings on the same antenna are ~=0 for the
        % same receiver. Convention: temp_RxTx
        temp_aa = SNR_map(i,j).SNR(1,1);
        temp_ab = SNR_map(i,j).SNR(2,1);
        temp_ba = SNR_map(i,j).SNR(1,2);
        temp_bb = SNR_map(i,j).SNR(2,2);
        
        if temp_aa ~= 0
            count_a = count_a +1;
        end
        
        if temp_ab ~= 0
            count_a = count_a +1;
        end  
        
        if temp_ba ~= 0
            count_b = count_b +1;
        end
        
        if temp_bb ~= 0
            count_b = count_b +1;
        end   
        
    end    
    if count_a > cubesat_type(j)
        a_is_conflicting(j) = 1;
    end    
    
    if count_b > cubesat_type(j)
        b_is_conflicting(j) = 1;
    end    
        
end   

conflicts = [a_is_conflicting ; b_is_conflicting];

end