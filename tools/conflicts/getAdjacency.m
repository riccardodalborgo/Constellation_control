function [A, snr_map] = getAdjacency(arguments, mask, cubesat_type)
%[A, snr_map] = getAdjacency(arguments, mask, cubesat_type)
%
% Computes the adjacency matrix of the communication graph modelling the 
% network.
%
% Inputs:
%
%   - arguments: 1-by-4N vector of the following stacked vectors
%           nodes_x: N-by-1 vector of x-axis coordinates 
%           nodes_y: N-by-1 vector of y-axis coordinates
%           theta_a: N-by-1 vector of the first transceiving directions 
%           theta_b: N-by-1 vector of the second transceiving directions 
%   - mask: N-by-N structures of 2-by-2 matrices of boolean values:
%           0 if the communication is not feasible
%           1 if the communication is feasible
%   - cubesat_type: 1-by-N vector of types 1/2/3 U
%
% Output:
%
%   - A: N-by-N adjacency matrix
%   - snr_map: N-by-N struct of 2-by-2 matrices of SNR values
%

    n = length(mask);
    A = zeros(n);
    SNR_map = getMapSNR(arguments(1:n)' , arguments(n+1:2*n)' , arguments(2*n+1:3*n)' , arguments(3*n+1:4*n)');

    % applying the mask
    for j = 1:n
        for i = 1:n
            snr_map(i,j).SNR = mask(i,j).SNR.*SNR_map(i,j).SNR;
        end
    end
   
    for k=1:2
        for j = 1:n    % scan rx
            % conflict found on j
            antenna =[];

            for i = 1:n
              antenna = [antenna ; snr_map(i,j).SNR(:,k)];
            end

            for i = 1:cubesat_type(j)
              [maxSNR, index] = max(antenna);
              if maxSNR ~=0
                  A(ceil(index/2),j) = 1;
                  A(j,ceil(index/2)) = 1;
                  antenna(index)=nan;  % it would be better nan
              else
                  antenna(index)=nan;
              end      
            end
        end
    end

end