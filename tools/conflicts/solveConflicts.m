function mask = solveConflicts(conflicts, arguments, mask, cubesat_type)
%mask = solveConflicts(conflicts, arguments, mask, cubesat_type)
%
% Solves eventual communication conflicts, signal by checkConflicts.
%
% Inputs:
%
%   - conflicts: 2-by-N vector of boolean flags (1 if conflict found)
%   - arguments: 1-by-4N vector of the following stacked vectors
%           nodes_x: N-by-1 vector of x-axis coordinates 
%           nodes_y: N-by-1 vector of y-axis coordinates
%           theta_a: N-by-1 vector of the first transceiving directions 
%           theta_b: N-by-1 vector of the second transceiving directions 
%   - mask: N-by-N structures of 2-by-2 matrices of boolean values:
%           0 if the communication is not feasible
%           1 if the communication is feasible
%   - cubesat_type: 1-by-N vector of types 1/2/3 U
%
% Output:
%
%   - mask: updated mask
%
% See also checkConflicts.

    n = length(conflicts);
    snr_map = getMapSNR(arguments(1:n)' , arguments(n+1:2*n)' , arguments(2*n+1:3*n)' , arguments(3*n+1:4*n)');

    
    % looking for conflicts
    for k=1:2 %antenna A or B
      for j = 1:n    % scan rx
          % conflict found on j
          if conflicts(k,j)    % if a RX has a conflict
            antenna =[];

            for i = 1:n
              antenna = [antenna ; snr_map(i,j).SNR(:,k)];
            end

            for i = 1:2*length(mask)-cubesat_type(j)
              [minSNR, index] = min(antenna);
              if minSNR ~=0
                  mask(ceil(index/2),j).SNR(:,k) = zeros(2,1);
                  mask(j,ceil(index/2)).SNR(k,:) = zeros(2,1);
                  antenna(index)=nan;  % it would be better nan
              else
                  antenna(index)=nan;
              end
            end

          end
      end
    end

    % applying the mask
    for j = 1:n
        for i = 1:n
            SNR_map(i,j).SNR = mask(i,j).SNR.*snr_map(i,j).SNR;
        end
    end
    
end
