function [m,n,p,D,tail,head,W] = Initialize(G)
%
%   Initialize  Transforms the graph G in the list-of-arcs form and intializes
%               the shortest path parent-pointer and distance arrays, p and D.
%
%   Inputs:
%           - G : digraph object
%
%   Output:
%           - m : number of edges
%           - n : number of nodes
%           - p : parent vector
%           - D : path cost vector, i.e. D(i) cost of the path from root 
%                 to node i
%           - tail: vector of edge tails
%           - head: vector of edge heads
%           - W: edge weights, i.e. W(i) weight of edge i with head head(i)
%                and tail tail(i)
%
% See also BellmanFord
%

tail =[];
head =[];
weight =[];
for i=1:height(G.Edges)
    h = G.Edges.EndNodes(i,1);
    t = G.Edges.EndNodes(i,2);
    w = G.Edges.Weight(i); 
    tail=[tail;t];
    head=[head;h];
    weight=[weight,w]; 
end 


W = G.Edges.Weight;
m = length(head);
n = size(G.Nodes,1);

p(1:n,1) = nan;        
D(1:n,1) = Inf;          

end 
    


