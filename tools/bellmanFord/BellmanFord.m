 function tree = BellmanFord(G,r)
%   BellmanFord  Basic form of the Bellman-Ford-Moore Minimum Cost Path algorithm.
%                Assumes G is a graph, with |N| = n, m directed edges. 
%                It constructs a MCST path tree with root r.
%                Complexity: O(mN)
%
%   Inputs:
%           - G : digraph object
%           - r : node ID of the root for the MCST (Minimum COst SPanning Tree)
%
%   Output:
%           - tree: the MCST for the given graph G with root r
%


[m,n,p,D,tail,head,W] = Initialize(G);
p(r)=0; D(r)=0;                        % Set the root of the SP tree (p,D)
    for iter = 1:n-1                   % Converges in <= n-1 iters if no 
        for arc = 1:m                  % O(m) optimality test. 
            u_index = head(arc); 
            v_index = tail(arc);
            duv = W(arc);
            if D(v_index) > D(u_index) + duv     % RELAX:
               D(v_index) = D(u_index) + duv;    %  
               p(v_index) = u_index;             % -> Update (p,D) 
              
            end
        end % for arc
    end %for iter

    
%% Return the tree in the form of a Graph structure
tree=digraph();

%root
[parent, index] = min(p);
p(index)=Inf;                                      %delete root from vector
tree = addnode(tree,G.Nodes);
%add other edges
for i=1:length(p)-1
    if W(i)~=Inf
        [parent, index] = min(p);
        tree = addedge(tree, parent,index, W(i));
    end
    p(index)=Inf;  
end  

  end
