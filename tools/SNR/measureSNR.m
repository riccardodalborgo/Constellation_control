function index = measureSNR(nodes_x, nodes_y, theta_a, theta_b, mask, types)
%index = measureSNR(nodes_x, nodes_y, theta_a, theta_b, mask, types)
%
% Computes the index measure for the N nodes in a formation. Recall that
% the index value is the average SNR between the potential connection of a
% satellite depending on its type.
%
% Inputs:
%
%   - nodes_x: N-by-1 vector of x-axis coordinates 
%   - nodes_y: N-by-1 vector of y-axis coordinates
%   - theta_a: N-by-1 vector of the first transceiving directions 
%   - theta_b: N-by-1 vector of the second transceiving directions 
%   - mask: N-by-N struct of 4-by-4 matrices of boolean values:
%           0 if the communication is not feasible
%           1 if the communication is feasible
%   - types: 1-by-N vector of types 1/2/3 U
%
% Output:
%
%   - index: 1-by-N vector of nodes computed index
%

SNR_map = getMapSNR(nodes_x, nodes_y, theta_a, theta_b);
n = length(SNR_map);

% apply the mask
for j = 1:n
    for i = 1:n
        SNR_map(i,j).SNR = mask(i,j).SNR.*SNR_map(i,j).SNR;
    end
end

for j = 1:n
    temp_a = zeros(n,1);
    temp_b = zeros(n,1);
        for i = 1:n

            temp_a(i) = max(SNR_map(i,j).SNR(1,1),SNR_map(i,j).SNR(2,1));
            temp_b(i) = max(SNR_map(i,j).SNR(1,2),SNR_map(i,j).SNR(2,2));
        end
        
        max_values_a = [0];
        max_values_b = [0];
        
        %scan the array to take the max number of good connection based on
        %satellite type
        
        for i=1:types(j)
            [max_values_a(i), index_a] = max(temp_a);
            [max_values_b(i), index_b] = max(temp_b);
            temp_a(index_a)=0;
            temp_b(index_b)=0;
        end

    index(j) = mean([max_values_a, max_values_b]);
end


end
