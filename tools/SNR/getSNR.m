function SNR = getSNR(tx, rx, theta_tx, theta_rx, stdev)
%SNR = getSNR(tx, rx, theta_tx, theta_rx, stdev)
%
% Computes the normalized SNR in the communication between two nodes. The
% receiving sensor is modelled through a Gaussian curve with variance specified by
% the user.
%
% Inputs:
%
%   - tx: 2D coordinates of the transmitting node
%   - rx: 2D coordinates of the receiving node
%   - theta_tx: Transmitting direction
%   - theta_rx: Receiving direction
%   - stdev: Standard deviation of the Gaussian modelling the receiving sensor
%       Default value = 0.25 (empirical)
%
% Outputs:
%
%   - SNR: The computed normalized SNR
%

  if nargin<5 || isempty(stdev)            
        stdev = 0.25;             % default value for standard deviation
  end

  %% Check if communication is feasible

  delta_theta = abs(theta_rx-theta_tx);
  
  if delta_theta <= pi/2 || delta_theta >= 3*pi/2
      SNR = 0;
      return
  end    
  
  % check the relative position between transmitter and receiver assigning
  % in what quadrant rx is w.r.t. tx in the XY plane
  if rx(1) >= tx(1) && rx(2) >= tx(2)
      q = 1;
  elseif rx(1) >= tx(1) && rx(2) <= tx(2)    
      q = 2;
  elseif rx(1) <= tx(1) && rx(2) <= tx(2)
      q = 3;
  else
      q = 4;
  end    

  % retrieve the tx/rx angular directions in the interval [0,2pi]
  theta_tx = wrapTo2Pi(theta_tx);  
  theta_rx = wrapTo2Pi(theta_rx);
  
  sweep = pi/3;
  gamma = pi/2 - abs(atan((rx(2)-tx(2))/(rx(1)-tx(1))));
  
  % consider a different computation of the displacements of tx and rx 
  % w.r.t. the quadrant assigned 
  switch q
      case 1
           if theta_tx < 0 || (theta_tx > pi/2 + sweep/2 && theta_tx < 2*pi-sweep/2)
               SNR = 0;
               return
           end
           % computation of singular displacements
           if theta_tx >= 2*pi-sweep/2
               disp_t = theta_tx - 2*pi - gamma;
           else
               disp_t = theta_tx - gamma;
           end   
           gamma = pi + gamma;
           disp_r = theta_rx - gamma;
     
      case 2
           if theta_tx < pi/2 - sweep/2 || theta_tx > pi + sweep/2
               SNR = 0;
               return
           end 
           % computation of singular displacements
           gamma = pi - gamma;
           disp_t = theta_tx - gamma;
           gamma = wrapTo2Pi(pi + gamma);
           if gamma == 2*pi && theta_rx < pi
              gamma = 0;
           end
           disp_r = theta_rx - gamma;
           
      case 3
           if theta_tx < pi - sweep/2 || theta_tx > 3*pi/2 + sweep/2
               SNR = 0;
               return
           end  
           % computation of singular displacements
           gamma = pi + gamma;
           disp_t = theta_tx - gamma;
           gamma = wrapTo2Pi(pi + gamma);
           if gamma == 2*pi && theta_rx < pi
              gamma = 0;
           end
           if theta_rx > pi
                disp_r = theta_rx - 2*pi - gamma;
           else
                disp_r = theta_rx - gamma;
           end
      case 4
          if theta_tx < 3*pi/2 - sweep/2 
               SNR = 0;
               return
          end 
          % computation of singular displacements
          gamma = 2*pi - gamma;
          disp_t = theta_tx - gamma;
          gamma = wrapTo2Pi(pi + gamma); 
          if gamma == 2*pi && theta_rx < pi
              gamma = 0;
          end
          disp_r = theta_rx - gamma;
      otherwise
          disp('Error in SNR computation');
  end    
  
  % compute angular distance w.r.t optimal position displacement_th = 0
  % as sum of singular displacements
   displacement_th = abs(disp_r) + abs(disp_t);
 
  % compute SNR
  SNR = getGaussian(displacement_th, 0, stdev);  
  max_SNR = getGaussian(0,0,stdev);

  % compute normalized SNR
  SNR = SNR/max_SNR;

  % reject too small value of SNR
      if SNR < 0.09    %(empirical value)
          SNR = 0;
      end
      
end
