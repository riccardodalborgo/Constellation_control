function SNR_map = getMapSNR(nodes_x, nodes_y,theta_a,theta_b)
%SNR_map = getMapSNR(nodes_x, nodes_y,theta_a,theta_b)
%
% Computes a structure in which there are the SNR values measured by 
% each pair of transceivers in a formation of N nodes.
%
% Inputs:
%
%   - nodes_x: N-by-1 vector of x-axis coordinates 
%   - nodes_y: N-by-1 vector of y-axis coordinates
%   - theta_a: N-by-1 vector of the first transceiving directions 
%   - theta_b: N-by-1 vector of the second transceiving directions 
%
% Output:
%
%   - SNR_map: N-by-N struct of 2-by-2 matrices of SNR values
%           

    nodes = [nodes_x, nodes_y];
    n = length(nodes);
   
    % rows: tx, cols: rx
    SNR_map = [];
   
    % fill the structure
    for i = 1:n
        for j = i+1:n
            SNR_map(i,j).SNR(1,1) = getSNR(nodes(i,:),nodes(j,:),theta_a(i),theta_a(j));
            SNR_map(i,j).SNR(1,2) = getSNR(nodes(i,:),nodes(j,:),theta_a(i),theta_b(j));
            SNR_map(i,j).SNR(2,1) = getSNR(nodes(i,:),nodes(j,:),theta_b(i),theta_a(j));
            SNR_map(i,j).SNR(2,2) = getSNR(nodes(i,:),nodes(j,:),theta_b(i),theta_b(j));
        end
        
        for j = 1:i-1
            SNR_map(i,j).SNR(1,1) = getSNR(nodes(i,:),nodes(j,:),theta_a(i),theta_a(j));
            SNR_map(i,j).SNR(1,2) = getSNR(nodes(i,:),nodes(j,:),theta_a(i),theta_b(j));
            SNR_map(i,j).SNR(2,1) = getSNR(nodes(i,:),nodes(j,:),theta_b(i),theta_a(j));
            SNR_map(i,j).SNR(2,2) = getSNR(nodes(i,:),nodes(j,:),theta_b(i),theta_b(j));
        end        
    end   
    
    % self-SNR measures are zero
    for i = 1:n       
        SNR_map(i,i).SNR = zeros(2);        
    end    
    
end