function index = evaluationIndex(nodes_x, nodes_y, theta_a, theta_b, mask)
%index = evaluationIndex(nodes_x, nodes_y, theta_a, theta_b, mask)
%
% Computes the index measure after the calibration phase for the N nodes 
% in a formation. Recall that the index value is the average SNR between 
% the established connection of a satellite.
%
% Inputs:
%
%   - nodes_x: N-by-1 vector of x-axis coordinates 
%   - nodes_y: N-by-1 vector of y-axis coordinates
%   - theta_a: N-by-1 vector of the first transceiving directions 
%   - theta_b: N-by-1 vector of the second transceiving directions 
%   - mask: N-by-N structures of 2-by-2 matrices of boolean values:
%           0 if the communication is not feasible
%           1 if the communication is feasible
%
% Output:
%
%   - index: 1-by-N vector of computed indices
%

SNR_map = getMapSNR(nodes_x, nodes_y, theta_a, theta_b);
n = length(SNR_map);



SNR_map = getMapSNR(nodes_x, nodes_y, theta_a, theta_b);
n = length(SNR_map);

% applying the mask
for j = 1:n
    for i = 1:n
        SNR_map(i,j).SNR = mask(i,j).SNR.*SNR_map(i,j).SNR;
    end
end

for j = 1:n
    temp = 0;
        
    for i = 1:n
            if SNR_map(i,j).SNR(1,1) ~= 0
                temp = [temp SNR_map(i,j).SNR(1,1)];
            end
            if SNR_map(i,j).SNR(1,2) ~= 0
                temp = [temp SNR_map(i,j).SNR(1,2)];
            end
            if SNR_map(i,j).SNR(2,1) ~= 0
                temp = [temp SNR_map(i,j).SNR(2,1)];
            end
            if SNR_map(i,j).SNR(2,2) ~= 0
                temp = [temp SNR_map(i,j).SNR(2,2)];
            end
    end
        
    index(j) = mean(temp);
end


end
