function y = getGaussian(x, m, d)
%y = getGaussian(x, m, d)
%
% Computes the value of a Gaussian curve specified by the user given 
% the x-axis coordinate. 
%
% Inputs:
%
%   - x: x-axis coordinate
%   - m: mean of the Gaussian curve
%   - d: standard deviation of the Gaussian curve
%
% Output:
%
%   - y: computed value of the Gaussian curve
%

    y = 1/(sqrt(2*pi*d^2))*exp((-0.5/d^2)*(x-m)^2);

end