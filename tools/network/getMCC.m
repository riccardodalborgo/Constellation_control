function [H] = getMCC(G)
 %   getMCC  Function to retrieve the Main Connected Component (MCC) of a
 %           given digraph G.
 %
 %   Input:
 %      - G : digraph object
 %  
 %   Output:
 %      - H : MCC of the initial graph G in the form of a digraph object 
 %
 
%Find all connected components
    bins = conncomp(G);
%Find the main connected components
    set = mode(bins);
    v = bins==set;
    ids = v.*G.Nodes.ID';
    
    MCC = [];
    for i = 1:length(ids)
        if ids(i) ~=0
           MCC = [MCC i];
        end    
    
    end
    
H = subgraph(G,MCC);

end