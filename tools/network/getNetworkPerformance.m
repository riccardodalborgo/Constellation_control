function performances=getNetworkPerformance(forest, snr_map)
 %   performances   The function evaluate network performance metrics for 
 %                  the given forest of communication trees.
 %
 %   Inputs:
 %      - forest : struct vector of communication trees
 %      - snr_map : N-by-N struct of 2-by-2 matrices of SNR values 
 %  
 %   Output:
 %      - performances : vector containing all computed informations for
 %                       the given network.
 %                       The fields of the vector are organized as in the
 %                       following:
 %                          [max_delay, avg_delay, min_snr, avg_snr]
 %                          

%-------------------------------------------------------------------------%
%   Evaluate Delay & Link Quality for the networks                        %
%-------------------------------------------------------------------------%
 
min_snr= nan;               % minimum snr value of used links in the network
lengths = [];               % vector off the length of every route
communication_snr=[];       %mean snr in a communication between source & target

for i=1:length(forest)      %for every tree
   
    % Select tree with root i
    tree=forest{i};
    
    for j=1:height(tree.Nodes) %for every target
        
        if i~=j     %for every node except root          
            %Compute path
            considered_path = shortestpath(tree,i,j);
            %Save length
            lengths = [lengths, length(considered_path)-1];
            
            %Compute snr for every link in path 
            links=[];
            for k=1:length(considered_path)-1
               tx = tree.Nodes.ID(considered_path(k));
               rx = tree.Nodes.ID(considered_path(k+1));
               % SNR value
               snr = sum(sum(snr_map(tx,rx).SNR));
              
               links=[links, snr] ;
               min_snr = min([min_snr, snr]);
            end   
            
            %Compute mean SNR for this path
            communication_snr= [communication_snr mean(links)];
        end  
    end
end

avg_snr = mean(communication_snr); 
avg_delay = mean(lengths); 
max_delay = max(lengths);

% Return the vector summarizing the performances
performances = [max_delay, avg_delay, min_snr, avg_snr];
  end