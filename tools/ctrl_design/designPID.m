function [Kp, Ki, Kd, TauL, C] = designPID(P, w_cross, phim, e, b, d)
%[Kp, Ki, Kd, TauL, C] = designPID(P, w_cross, phim, e, b, d)
%
% Computes the parameters of the PID controller designed to satisfy
% the given specifications.
%
% Inputs:
%
%   - P: transfer function of the plant 
%   - w_cross: desired crossing frequency
%   - phim: desired phase margin
%   - e: desired steady-state error
%   - b: Ti/Td ratio 
%   - d: boolean to require perfect steady state tracking with input
%   disturbance rejection (d=1)
%
% Outputs:
%
%   - Kp: proportional gain
%   - Ki: integral gain
%   - Kd: derivative gain
%   - TauL: pole time constant for the derivative term
%   - C: transfer function of the PID controller
%

[P_mag, P_phi] = bode(P, w_cross);
P_phim = 180 + P_phi;
dcP = dcgain(P);
C_phi = phim - P_phim;
C_mag = 1/P_mag;
deg2rad = pi/180;

if(d==true)

    if((P_phim - 90)>=phim)                     % I
        Ki = w_cross/P_mag;
        Kp = 0;
        Kd = 0;
        TauL = 0;
        C = tf(Ki,[1,0]);
        return 

    elseif((P_phim - 90)<phim && phim<=P_phim) %P or PI controller
        Kp = 1/P_mag;
        ess = 100/(1+dcP*Kp);
        if (ess < e)                       % P 
            Ki = 0;
            Kd = 0;
            TauL = 0;
            C = tf(Kp);
            return 
        end
        Kp = C_mag*cos(deg2rad*C_phi);       %PI
        Ki = -C_mag*w_cross*sin(deg2rad*C_phi);
        Kd = 0;
        TauL = 0;
        C = tf([Kp,Ki],[1,0]);
        return 
 
    elseif(P_phim<phim)                       
        Kp = C_mag*cos(deg2rad*C_phi);
        Kd = C_mag*sin(deg2rad*C_phi)/w_cross;
        TauL = 1/(w_cross*5);
        ess = 100/(1+dcP*Kp);
        if (ess < e)                          % PD
            Ki = 0;
            C = tf([Kp*TauL + Kd, Kp],[TauL,1]);
            return 
        end
        Ki = 0.5*C_mag*w_cross * (-sin(deg2rad*C_phi) + sqrt((sin(deg2rad*C_phi))^2 + 4*(cos(deg2rad*C_phi))^2/b));
        Kd = (Kp)^2/(b*Ki);
        C = tf([Kp*TauL + Kd, Kp + Ki*TauL, Ki],[TauL, 1, 0]);
     return
     
    else
        Kp = 0;
        Ki = 0;
        Kd = 0;
        TauL = 0;
        C = 0;   
    end
    
else
    
    if((P_phim - 90)>=phim)                     % I
        Ki = w_cross/P_mag;
        Kp = 0;
        Kd = 0;
        TauL = 0;
        C = tf(Ki,[1,0]);
        return 
     
    elseif((P_phim - 90)<phim && phim<=P_phim)  %PI
        Kp = C_mag*cos(deg2rad*C_phi);       
        Ki = -C_mag*w_cross*sin(deg2rad*C_phi);
        Kd = 0;
        TauL = 0;
        C = tf([Kp,Ki],[1,0]);
        return 
        
    elseif(P_phim<phim)                         %PID        
        Kp = C_mag*cos(deg2rad*C_phi);
        Ki = 0.5*C_mag*w_cross * (-sin(deg2rad*C_phi) + sqrt((sin(deg2rad*C_phi))^2 + 4*(cos(deg2rad*C_phi))^2/b));
        Kd = (Kp)^2/(b*Ki);
        TauL = 1/(w_cross*5);
        C = tf([Kp*TauL + Kd, Kp + Ki*TauL, Ki],[TauL, 1, 0]);
     return
     
    else
        Kp = 0;
        Ki = 0;
        Kd = 0;
        TauL = 0;
        C = 0;
    end
    
end