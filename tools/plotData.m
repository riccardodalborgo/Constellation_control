function plotData(dataName)
%plotData(dataName, iteration)
%
% Plots simulation data.
%
% Inputs:
%
%   - dataName: file name of the simulation result stored in
%   /simulation_results
%
addpath ('./simulation_results');

load (dataName);

name = 'Initial search';
init_idx = datasim.init_idx;
nodes = datasim.nodes;
A = datasim.A;

theta_history = datasim.firstSim.theta_history;
theta_a = datasim.firstSim.theta_a;
theta_b = datasim.firstSim.theta_b;
measured_idx = datasim.firstSim.measured_idx;

fprintf('Initial SNR: \n');
disp(init_idx);

animation_plot(name,A, theta_history, nodes, theta_a, theta_b, measured_idx)
for i=1:length(datasim.secondSim)

    name = strcat('Conflict rejection ', num2str(i));
    theta_history = datasim.secondSim(i).theta_history;
    theta_a = datasim.secondSim(i).theta_a;
    theta_b = datasim.secondSim(i).theta_b;
    measured_idx = datasim.secondSim(i).measured_idx;

    animation_plot(name,A, theta_history, nodes, theta_a, theta_b, measured_idx)

end

