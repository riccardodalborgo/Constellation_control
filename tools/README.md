# Constellation Control/tools
Folder containing all the project functions, organized as:

* **tools/SNR**: folder containing all the functions relative to the computation of the SNR in the communication between agents.
* **tools/animation**: folder containing all the functions for data plotting
* **tools/bellmanFord**: folder containing all the functions implementing the Bellman-Ford algorithm
* **tools/conflicts**: folder containing all the functions to handle the communication conflict problem.
* **tools/ctrl_design**: folder containing the function to design the PID controller used in the system.
* **tools/graphs**: folder containing all the functions to plot the analysis results on the network after the simulation phase.
* **tools/network**: folder containing all the functions to evaluate the performances of the network obtained after the simulation phase.
* **tools/loadParameters.m**: loads on the main MATLAB workspace all the parameters necessary for the simulation. Invoked by `main_sim.m`.
* **tools/plotData.m**: plots simulation data. Invoked by `main_sim.m`.
* **tools/simulate.m**: performs the simulation. Invoked by `main_sim.m`.

