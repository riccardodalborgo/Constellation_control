disp('Start simulation')
% Prepare data structure
simulation_data = [];
% Save nodes
simulation_data.nodes = nodes;

% Random orientation of tx, rx at least at pi/2 from tx, angles in the [0,2pi] interval
theta_a = wrapTo2Pi(2*pi*rand(n,1));
theta_b = wrapTo2Pi(theta_a + pi/2 +pi/2*rand(n,1));

% Save data
simulation_data.firstSim.theta_a = theta_a;
simulation_data.firstSim.theta_b = theta_b;

%% Simulink parameters

% Rotation direction
k_theta = (-1).^(round(rand(n,1)));
% Blind search step size
blind_step = round(2*rand(n,1)+3);

% Compute clean mask
for i=1:n
    for j=1:n
        fresh_mask(i,j).SNR = ones(2);
        if i==j
           fresh_mask(i,j).SNR = zeros(2);
        end
    end
end

mask = fresh_mask; % clean mask

% Index computation
init_idx = measureSNR(nodes(:,1),nodes(:,2),theta_a, theta_b, mask,cubesat_type);

% Save
simulation_data.init_idx = init_idx;

%% Alignment simulation

warning('off','all');
sim('multisatSNR');

disp('Initial search: DONE')

% Save
simulation_data.firstSim.theta_history = theta_history;
simulation_data.firstSim.measured_idx = measured_idx;
simulation_data.firstSim.arguments = snr_map_arguments;

%% Conflict Resolution

final_measure = measured_idx(:,:,end);
arguments = snr_map_arguments(end,:);  %SNR_map

mask = fresh_mask; % clean mask

% Compute SNR_map
SNR_map = getMapSNR(arguments(1:n)' , arguments(n+1:2*n)' , arguments(2*n+1:3*n)' , arguments(3*n+1:4*n)');
% Check the presence of conflictual transmissions
conflicts = checkConflicts(SNR_map,cubesat_type);

% Solve eventual conflicts
if max(max(conflicts)) == 1 % enters if conflicts ~= zeros(2,n)
    mask = solveConflicts(conflicts,arguments,mask,cubesat_type);
end

%% Sequential conflict resolution
n_resolutions = 0;

if max(max(conflicts)) ~= 1
    simulation_data.secondSim = [];
    disp('No conflicts found.')
else
    disp('Conflict found.')

    warning('off','all');
    while max(max(conflicts)) == 1 && n_resolutions < 2
        n_resolutions = n_resolutions + 1;

        % Start simulation from end of the previous one
        theta_a = theta_a + theta_history.signals.values(:,:,end);
        theta_b = theta_b + theta_history.signals.values(:,:,end);

        % Save
        simulation_data.secondSim(n_resolutions).theta_a = theta_a;
        simulation_data.secondSim(n_resolutions).theta_b = theta_b;

        % Update rotations and angular velocity
        % Rotation direction
        k_theta = (-1).^(round(rand(n,1)));
        % Blind search step size
        blind_step = round(2*rand(n,1)+3);

        sim('multisatSNR');
        disp('Conflict Step Resolved')

        % Save
        simulation_data.secondSim(n_resolutions).theta_history = theta_history;
        simulation_data.secondSim(n_resolutions).measured_idx = measured_idx;
        simulation_data.secondSim(n_resolutions).arguments = snr_map_arguments;

        % Conflicts resolution
        final_measure = measured_idx(:,:,end);
        arguments = snr_map_arguments(end,:);

        % Compute SNR_map
        SNR_map = getMapSNR(arguments(1:n)' , arguments(n+1:2*n)' , arguments(2*n+1:3*n)' , arguments(3*n+1:4*n)');
        % Check the presence of conflictual transmissions
        conflicts = checkConflicts(SNR_map,cubesat_type);

        mask = fresh_mask; % clean mask

        % Solve eventual conflicts
        if max(max(conflicts)) == 1 % enters if conflicts ~= zeros(2,n)
            mask = solveConflicts(conflicts,arguments,mask,cubesat_type);
        end

    end
end

% Save
simulation_data.n_resolutions = n_resolutions;

%% Remove last conflicts

idx = evaluationIndex(arguments(1:n)' , arguments(n+1:2*n)' , arguments(2*n+1:3*n)' , arguments(3*n+1:4*n)',mask);
[A, snr_map] = getAdjacency(arguments,mask, cubesat_type);

%% Save data

simulation_data.A = A;
simulation_data.snr_map = snr_map;
simulation_data.idx = idx;

