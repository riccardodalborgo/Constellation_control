function animation_plot(name, A, theta_history, nodes, theta_a, theta_b, measured_idx)
%animation_plot(name, A, theta_history, nodes, theta_a, theta_b, measured_idx)
%
% Plots the animation of the formation dynamic.
%
% Inputs:
%
%   - name: name of the plot window
%   - A: adjacency matrix of the communication graph
%   - theta_history: record of all the transceiving directions during the simulation 
%   - nodes: N-by-2 vector of nodes coordinates 
%   - theta_a: N-by-1 vector of the initial first transceiving directions 
%   - theta_b: N-by-1 vector of the initial second transceiving directions 
%   - measured_idx: 1-by-N vector of computed indices
%

G = graph(A);
f = figure('Name', name, 'units','normalized','outerposition',[0 0 1 1]);
theta_animation = theta_history.signals.values(:,:,:);

    for i = 1:round(length(theta_animation)/100):length(theta_animation)

       % graph animation subplot
       subplot(1,3,[1 2])
       H = plot(G,'XData', nodes(:,1),'YData',nodes(:,2));
       nicePlotting('','','')
       hold on; grid on;

       quiver(nodes(:,1),nodes(:,2),sin(theta_a + theta_animation(:,:,i)),cos(theta_a + theta_animation(:,:,i)),'AutoScaleFactor',0.1,'MaxHeadSize',0.5);
       quiver(nodes(:,1),nodes(:,2),sin(theta_b + theta_animation(:,:,i)),cos(theta_b + theta_animation(:,:,i)),'AutoScaleFactor',0.1,'MaxHeadSize',0.5);

       legend('Nodes','antenna a','antenna b', 'Location', 'best');
%        ylim([0,1])
%        xlim([0,1])
       axis equal

       % subplot of the snr table

       data = measured_idx(:,:,i)';
       colnames = {'Index'};
       t = uitable(f, 'Data', data, 'ColumnName', colnames,'Position',[0, 0, 1, 1]);

       % to be able to subplot a table
       pos = get(subplot(1,3,3),'position');

       delete(subplot(1,3,3))
%        nicePlotting('','','')
       set(t,'units','normalized')
       set(t,'position',pos)

       drawnow

       snapnow
       hold off;

    end


end