function loadParameters(graph_filename, params)
%loadParameters(graph_filename, params)
%
% Loads the parameters for the simulation in the main workspace.
%
% Inputs:
%
%   - graph_filename: file name of the considered graph, stored in /data
%   - params: tuning parameters for the simulation
%
%% Graph extraction
path = strcat('./data/',graph_filename);
graph = load(path);

% Adjacency matrix extraction
A = graph.graph.graph_props.adjMat;

% Extracting positions of the nodes (For SNR computation)
nodes = graph.graph.randProps.nodeLoc;

% Size of the graph
n = size(nodes);
n = n(1);

%% Get user defined params
alpha_gain= params.alpha_gain;
noiseVar = params.IMU_noiseVar;
cubesat_type= round(2*rand(1,n)+1);

%% Physical parameters
m = 1.33;                       % mass
l = 0.1;                        % side length
J = (1/6)*m*l^2;                % inertia
J_mat = diag(J*cubesat_type);

%% TF
n1 = 1;

for i = 1:n 
    d1 = [J_mat(i,i) 0 0];
    sat_tf(i) = tf(n1,d1);
end

%% Controller Design

% position controller specs & params
pctrl.ts =5;                                                   %   desired settling time (at 5%)
pctrl.Mp = 0.1;                                                %   desired overshoot
pctrl.e = 0;
pctrl.alpha = 4;

% get specs for loop tf
d = log(1/pctrl.Mp) / sqrt(pi^2 + log(1/pctrl.Mp)^2);           %   damping factor
pctrl.wgc = 3/d/pctrl.ts;                                       %   gain xover freq [rad/s]
pctrl.phim = 180/pi * atan(2*d/sqrt(sqrt(1+4*d^4)-2*d^2));      %   phase margin [deg]
pctrl.tr = 0.34/pctrl.wgc;

% controller design
for i = 1:n  
    [kp(i), ki(i), kd(i), Tau(i), C(i)] = designPID(sat_tf(i), pctrl.wgc, pctrl.phim, pctrl.e, pctrl.alpha, 1);
    kw(i) = 2.5;
end

%% Save parameters to workspace

% Graph
assignin('base', 'A', A);
assignin('base', 'nodes', nodes);
assignin('base', 'n', n);
assignin('base', 'cubesat_type', cubesat_type);

% Simulink parameters
assignin('base', 'alpha_gain', alpha_gain);
assignin('base', 'noiseVar', noiseVar);
% TF
assignin('base', 'J', J);
assignin('base', 'J_mat', J_mat);

% Controller
assignin('base', 'kp', kp);
assignin('base', 'ki', ki);
assignin('base', 'kd', kd);
assignin('base', 'Tau', Tau);
assignin('base', 'C', C);
assignin('base', 'kw', kw);
end