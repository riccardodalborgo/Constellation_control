function weightEvolution(pth)
% weightEvolution  Function for plotting the MCC/Nodes ratio for the 
%                  results of the routine network analysis.m in dir 
%                  described by 'pth'
% Inputs:
%      - pth: path of the directory
%    

%% Load
files = dir(pth);

for K = 3 : length(files)
    load(strcat(pth,files(K).name))
    data.performance(K-2,:) = performance;
end

%% Params

blue = [0.25,0.58,0.95];
green = [0.21, 0.72, 0.01];
orange = [1, 0.5, 0];
%% Load

files = dir(pth);

for K = 3 : length(files)
    load(strcat(pth,files(K).name))
    data.performance(K-2,:) = performance;
end
%% Reorder Data wrt MCC
% for i=1:length(data.performance)
%     [k index]= min([data.performance.MCC]);
%     newData.performance(i,:) = data.performance(index,:);
%     data.performance(index).MCC= inf;
% end
% data=newData;

%% Reorder Data wrt MCC (no MCC cardinality duplicates)
z=1; % Index for fill up new data structure

    [k index]= min([data.performance.MCC]);
    newData.performance(z,:) = data.performance(index,:);
    data.performance(index).MCC= inf;
    z=z+1;
    
for i=1:length(data.performance)-1
    [k index]= min([data.performance.MCC]);
    if ~ismember(k,[newData.performance.MCC])
        newData.performance(z,:) = data.performance(index,:);
        z=z+1;
    end
    data.performance(index).MCC= inf;
end
data=newData;
%% index evolution

% Create data
n_nodes = [data.performance.n_nodes]';

% IDX ---------------------------------------------------------------------
performance_idx= [data.performance.idx];

IDX_mean_delay=[];
%Extract max delay
for i=1:length(performance_idx)/4
    IDX_mean_delay(i) = performance_idx(4*i-2);
end

IDX_mean_snr=[];
%Extract max delay
for i=1:length(performance_idx)/4
    IDX_mean_snr(i)= performance_idx(4*i);
end

% SNR --------------------------------------------------------------------- 
performance_snr= [data.performance.snr];
 
SNR_mean_delay=[];
%Extract max delay
for i=1:length(performance_snr)/4
    SNR_mean_delay(i) = performance_snr(4*i-2);
end

SNR_mean_snr=[];
%Extract max delay
for i=1:length(performance_idx)/4
    SNR_mean_snr(i)= performance_snr(4*i);
end
% NB ---------------------------------------------------------------------- 
performance_nb= [data.performance.nb];

NB_mean_delay=[];
%Extract max delay
for i=1:length(performance_nb)/4
    NB_mean_delay(i) = performance_nb(4*i-2);
end
 
NB_mean_snr=[];
%Extract max delay
for i=1:length(performance_nb)/4
    NB_mean_snr(i)= performance_nb(4*i);
end

x_axis=[1:size(data.performance(:,:),1)];

%% PLOT 1 ----------------------------------------------------------------- 

% Create a plot with 2 y axes using the plotyy function
figure('Name','SNR vs IDX')
[hAx,hLine1,hLine2] = plotyy(x_axis, SNR_mean_delay-IDX_mean_delay,...
    x_axis, SNR_mean_snr-IDX_mean_snr ,'bar','plot');

%Axes Property
nicePlotting('', '$M$','$\Delta t_{AVG}$ ');
set(gca,'fontsize',30);
ylim([-0.5 0.5]);
yticks([-0.5:0.2:0.5]);
xticks(0:length(data.performance));
set(gca, 'XTickLabel', {0,data.performance.MCC});
set(gca,'TickLabelInterpreter', 'latex');

axes(hAx(2));
set(gca,'fontsize',30);
ylim([-0.05 0.05]);
yticks([-0.05:0.02:0.05]);
nicePlotting('', '','$\Delta SNR_{AVG}$');
set(hAx,{'ycolor'},{blue;orange});
set(gca,'TickLabelInterpreter', 'latex');

%Property line 1
hLine1.FaceColor = blue;
%Property line 2
hLine2.Color = orange;
hLine2.LineStyle='--';
hLine2.LineWidth = 2.5;
hLine2.Marker = 'o';
hLine2.MarkerSize=10;
hLine2.MarkerFaceColor = orange;


%% PLOT 2 ----------------------------------------------------------------- 

% Create a plot with 2 y axes using the plotyy function
figure('Name','NB vs IDX')
[hAx,hLine1,hLine2] = plotyy(x_axis, NB_mean_delay-IDX_mean_delay,...
    x_axis, NB_mean_snr-IDX_mean_snr ,'bar','plot');

%Axes Property
nicePlotting('', '$M$','$\Delta t_{AVG}$ ');
set(gca,'fontsize',30);
ylim([-0.5 0.5]);
yticks([-0.5:0.2:0.5]);
xticks(0:length(data.performance));
set(gca, 'XTickLabel', {0,data.performance.MCC});
set(gca,'TickLabelInterpreter', 'latex');

axes(hAx(2));
set(gca,'fontsize',30);
ylim([-0.05 0.05]);
yticks([-0.05:0.02:0.05]);
nicePlotting('', '','$\Delta SNR_{AVG}$');
set(hAx,{'ycolor'},{blue;orange});
set(gca,'TickLabelInterpreter', 'latex');

%Property line 1
hLine1.FaceColor = blue;
%Property line 2
hLine2.Color = orange;
hLine2.LineStyle='--';
hLine2.LineWidth = 2.5;
hLine2.Marker = 'o';
hLine2.MarkerSize=10;
hLine2.MarkerFaceColor = orange;

end