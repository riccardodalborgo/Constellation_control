function performanceBar(file)
% performanceBar  Function for plotting the performance parameters for a 
%                 specified graph
%
% Inputs:
%      - file: the mat file output of the routine network_analysis.m
%    
%% Params

blue = [0.25,0.58,0.95];
green = [0.21, 0.72, 0.01];
orange = [1, 0.5, 0];

%% Data
load(file)

x  = 1:3;
max_delay = [performance.snr(1),performance.idx(1),performance.nb(1)];
mean_SNR = [performance.snr(4),performance.idx(4),performance.nb(4)];

meas1=[max_delay'];
meas2=[ mean_SNR'];

%% Plot
figure('Name', 'Weight evaluation- Max delay')

bar(meas1, 'FaceColor', blue);
nicePlotting('','','$t_{WC}$');
ylim([0 16]);
set(gca,'TickLabelInterpreter', 'latex');
set(gca, 'XTickLabel', {'SNR','IDX', 'D'});
ax=gca;
ax.YGrid = 'on';
ax.GridLineStyle = '--';
figure('Name', 'Weight evaluation- Max delay');
bar(meas2, 'FaceColor', orange);
nicePlotting('','','$SNR_{AVG}$');
ylim([0.5 0.6]);
set(gca,'TickLabelInterpreter', 'latex');
set(gca, 'XTickLabel', {'SNR','IDX', 'D'});
ax=gca;
ax.YGrid = 'on';
ax.GridLineStyle = '--';

end