function MCCcurveEstimate(pth)
% MCCcurveEstimate  Function for computing & plotting the MCC curve estimate
%
% Input:
%       - pth: path of the folder containing all the mat file, outputs of the
%            function network_analysis.m

%% Params

blue = [0.25,0.58,0.95];
green = [0.21, 0.72, 0.01];
orange = [1, 0.5, 0];
orange2 = [0.8588 0.4275 0];
%% Load
files = dir(pth);

for K = 3 : length(files)
    load(strcat(pth,files(K).name))
    data.performance(K-2,:) = performance;
end

%% Data

% Create data
n_nodes = [data.performance.n_nodes]';
MCC = [data.performance.MCC]';

%% Linear Model Fit
% figure()
% FO = fit(n_nodes, MCC, 'poly1' );
% plot(n_nodes, MCC,'*')
% hold on
% plot(FO)
% xlim([0,55])

%% Exponential Model Fit
figure()
hold on
FO2=fit(n_nodes, MCC, 'exp1' )
x=[-2:55];
y=FO2.a*exp(FO2.b*x);
plot(x,y, 'Color', blue, 'LineWidth', 2)
plot(n_nodes, MCC,'o','MarkerSize',8,...
    'MarkerEdgeColor',orange2,...
    'MarkerFaceColor',orange)

xlim([0, 55])
nicePlotting('','$N$','$M$');
xticks([0:10:50]);
l = legend('Fit Curve', 'Data', 'Location', 'northwest');
set(l, 'interpreter', 'latex');

end