function alignmentPhases(fileName)
% alignmentPhases  plot the alignment phases for a given network described 
%                  by the mat file in 'fileName'.
%                  The function highlights the actual conflicts in the
%                  network.
% Input:
%       - fileName: mat file produced by the simulation routine main_sim.m
%

%% Params
blue = [0.25,0.58,0.95];
green = [0.21, 0.72, 0.01];
orange = [1, 0.5, 0];

%% Data
load(fileName)

% Compute clean mask
n = length(datasim.nodes);
for i=1:n
    for j=1:n
        mask(i,j).SNR = ones(2);
        if i==j
           mask(i,j).SNR = zeros(2);
        end
    end
end

% Initial Configuration--------------------------------------------------

[A, snr_map] = getAdjacency(datasim.firstSim.arguments(1,:), mask, datasim.cubesat_type);
initial_graph = graph(A);

figure()
p = plot(initial_graph,'EdgeColor', blue,'EdgeAlpha',1,'LineStyle','--','LineWidth',2,...
    'MarkerSize',8,'XData',datasim.nodes(:,1),'YData',datasim.nodes(:,2));
p.NodeCData = datasim.cubesat_type;
colormap('copper');
c=colorbar;
c.Label.String = 'CubeSat type';
c.Label.Interpreter = 'latex';
set(c,'YTick',[1 2 3 ]);

nicePlotting('', '', '');
ylim([-0.05, 1.05]);
xlim([-0.05, 1.05]);
set(gca,'xtick',[], 'ytick',[]);
set(gca,'Visible','off');

% Intermediate Configuration----------------------------------------------
[A, snr_map] = getAdjacency(datasim.secondSim(1).arguments(1,:), mask, datasim.cubesat_type);
intermediate_graph = graph(A);

figure()
p = plot(intermediate_graph,'EdgeColor', blue,'EdgeAlpha',1,'LineStyle','--','LineWidth',2,...
    'MarkerSize',8,'XData',datasim.nodes(:,1),'YData',datasim.nodes(:,2));

p.NodeCData = datasim.cubesat_type;
colormap('copper');
c=colorbar;
c.Label.String = 'CubeSat type';
c.Label.Interpreter = 'latex';
set(c,'YTick',[1 2 3 ]);

nicePlotting('', '', '');
ylim([-0.05, 1.05]);
xlim([-0.05, 1.05]);
set(gca,'xtick',[], 'ytick',[]);
set(gca,'Visible','off');

conflicts = checkConflicts(snr_map,datasim.cubesat_type);

% looking for conflicts
for k=1:2 %antenna A or B
  for j = 1:n    % scan rx
      % conflict found on j
      if conflicts(k,j)    % if a RX has a conflict
        antenna =[];

        for i = 1:n
          antenna = [antenna ; snr_map(i,j).SNR(:,k)];
        end
         for i = 1:2*length(mask)-1
          if antenna(i) ~=0
             highlight(p,[ceil(i/2),j],...
                'EdgeColor',orange,'LineStyle','--','LineWidth',2)
          end
        end

      end
  end
end

% Final Configuration ---------------------------------------------------
[A, snr_map] = getAdjacency(datasim.secondSim(length(datasim.secondSim)).arguments(end,:), mask, datasim.cubesat_type);
final_graph = graph(A);

figure()
p = plot(final_graph,'EdgeColor', blue,'EdgeAlpha',1,'LineStyle','-','LineWidth',2,...
    'MarkerSize',8,'XData',datasim.nodes(:,1),'YData',datasim.nodes(:,2));

p.NodeCData = datasim.cubesat_type;
colormap('copper');
c=colorbar;
c.Label.String = 'CubeSat type';
c.Label.Interpreter = 'latex';
set(c,'YTick',[1 2 3 ]);

nicePlotting('', '', '');
ylim([-0.05, 1.05]);
xlim([-0.05, 1.05]);
set(gca,'xtick',[], 'ytick',[]);
set(gca,'Visible','off');
end