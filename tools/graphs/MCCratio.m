function MCCratio(pth)
% MCCratio  Function for plotting the MCC/Nodes ratio for the results of the network
%           analysis in dir described by 'pth'
% Input:
%      - pth: path of the folder containing all the mat file, outputs of the
%             function network_analysis.m

%% Params

blue = [0.25,0.58,0.95];
green = [0.21, 0.72, 0.01];
orange = [1, 0.5, 0];
%% Load
files = dir(pth);

for K = 3 : length(files)
    load(strcat(pth,files(K).name))
    data.performance(K-2,:) = performance;
end

%% Number of nodes/MCC ratio

% Create data
n_nodes = [data.performance.n_nodes]';
MCC = [data.performance.MCC]';

% Create a stacked bar chart using the bar function
graph_MCC = figure('Name','nodes/MCC ratio');
plotMCC = bar([1:size(data.performance(:,:),1)], [MCC n_nodes-MCC], 0.5, 'stack');

nicePlotting('','$N$','');
ylabel('','FontSize',30);
set(plotMCC, {'FaceColor'},{orange; blue});

% Adjust the axis limits
ylim([0,55]);
yticks([0:10:55]);
ax=gca;
ax.YGrid = 'on';
ax.GridLineStyle = '-';
set(gca, 'XTickLabel', {data.performance.n_nodes});

% Add a legend
l=legend( 'MCC', 'Satellites','Location','northwest');
set(l, 'interpreter', 'latex');
end