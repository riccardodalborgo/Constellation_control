function ax = nicePlotting(title, x_label, y_label)
% nicePlotting set some params for the current axes of the plot.
%
% Inputs:
%       - title: title string
%       - x_label: x axis label
%       - y_label: y axis label

    ax = gca; % get the current axes
    ax.FontUnits = 'points';
    ax.FontSize = 22;
    ax.Title.Interpreter = 'latex';
    ax.Title.String = title;
    ax.XLabel.Interpreter = 'latex';
    ax.XLabel.String = x_label;
    ax.YLabel.Interpreter = 'latex';
    ax.YLabel.String = y_label;
    FontSize = 18;
    ax.FontUnits = 'points';
    ax.GridLineStyle = ':'; 
    ax.LineWidth = 1.5;
    ax.YLabel.Interpreter = 'latex';
    ax.TickDir = 'in';
end