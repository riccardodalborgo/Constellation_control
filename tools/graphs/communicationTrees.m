function communicationTrees(file, index)
% communication_trees  plot the the trees of the three weighted forests
%                       with root index for the network configuration obtained 
%                       in the simulation file.
% Inputs:
%       - file: mat file produced by the simulation routine main_sim.m
%       - index: root of the trees
%% Choose data
load(file);

G = digraph(datasim.A);
G_undirected = graph(datasim.A);
nodes = datasim.nodes; 
idx = datasim.idx;
snr_map = datasim.snr_map;

D = outdegree(G);
n = height(G.Nodes);
m = height(G.Edges);

%% Plot network
fig = figure('Name','Network after alignment phase');
p = plot(G_undirected,'XData',nodes(:,1),'YData',nodes(:,2));
G.Nodes.ID = cellfun(@str2num, p.NodeLabel');
G.Nodes.NodeOutDegree = D;
p.NodeCData = G.Nodes.NodeOutDegree;
c = colorbar;

c.Label.String = 'Node outdegree';
nicePlotting('Aligned Network', '', '');
ylim([-0.05, 1.05]);
xlim([-0.05, 1.05]);

%% Network weighted by neighbor degree
G_nb = G;

% Adding weigths as 1/# of neighbours 
for i = 1:m
      G_nb.Edges.Weight(i) = 1/G.Nodes.NodeOutDegree(G.Edges.EndNodes(i,2));
end  

% Extraction of main connected component (MCC)
G_nb = getMCC(G_nb);
n_nb = height(G_nb.Nodes);

%% Network weighted by link snr
G_snr = G;

% Adding weigths as 1/snr(Rx)
for i = 1:m
    rx = G_snr.Edges.EndNodes(i,2);
    tx = G_snr.Edges.EndNodes(i,1);
    snr = sum(sum(snr_map(tx,rx).SNR));   % using the fact only one differs form 0
    G_snr.Edges.Weight(i) = 1/snr;
end

% Extraction of main connected component (MCC)
G_snr = getMCC(G_snr);
n_snr = height(G_snr.Nodes);

%% Network weighted by index
G_idx = G;
% Adding weigths as 1/idx 
for i = 1:m
    rx = G_idx.Edges.EndNodes(i,2);
    G_idx.Edges.Weight(i) = 1/idx(rx);   
end  

% Extraction of main connected component (MCC)
G_idx = getMCC(G_idx);
n_idx = height(G_idx.Nodes);

%-------------------------------------------------------------------------%
%           STARTING NETWORK EVALUATION                                   %
%                                                                         %
%-------------------------------------------------------------------------%
%% Compute communications tree for nb weight
for root=1:n_nb
    %Compute tree
    tree_nb = BellmanFord(G_nb,root);

    %Populate the forest
    forest_nb(root,1)  = {tree_nb};

end

%Evaluate performance
     performance_nb = getNetworkPerformance(forest_nb,snr_map);

%% Compute communications tree for snr weight
for root=1:n_snr
    %Compute tree
    tree_snr = BellmanFord(G_snr,root);

    %Populate the forest
    forest_snr(root,1)  = {tree_snr};
end

%Evaluate performance
    performance_snr = getNetworkPerformance(forest_snr, snr_map);

%% Compute communications tree for idx weight
for root=1:n_snr
    %Compute tree
    tree_idx = BellmanFord(G_idx,root);

    %Populate the forest
    forest_idx(root,1)  = {tree_idx};
end

%Evaluate performance
    performance_idx = getNetworkPerformance(forest_idx,snr_map);

%% Display the data


performance.n_nodes= n;
performance.MCC=n_nb;
performance.nb=performance_nb;
performance.snr=performance_snr;
performance.idx=performance_idx;

% Print results
disp(performance);
  

%% Path Comparison

% for i=1:17
%     tree_snr= forest_snr{i};
%     tree_idx= forest_idx{i};
%     tree_nb= forest_nb{i};
% 
%     figure()
% 
%     subplot(1,3,1)
%     plot(tree_snr)
% 
%     subplot(1,3,2)
%     plot(tree_idx)
% 
%     subplot(1,3,3)
%     plot(tree_nb)
% end
%% SubPlot
blue = [0.25,0.58,0.95];
green = [0.21, 0.72, 0.01];
orange = [1, 0.5, 0];

% Consider only the specified communication trees
i=index;

tree_snr= forest_snr{i};
tree_idx= forest_idx{i};
tree_nb= forest_nb{i};


% SNR----------------------------------------------------------------------
figure()

H = plot(tree_snr,'EdgeAlpha',0.7,'EdgeColor', blue,'LineStyle','-','LineWidth',1.2,...
    'MarkerSize',8, 'NodeColor', blue);
nicePlotting('SNR','','$t$');


ax = gca;
ax.XColor = 'white';
set(gca,'TickLabelInterpreter', 'latex');
set(gca,'xtick',[]);
set(gca,'ytick',[]);

ax.YGrid= 'on';

highlight(H,[4],'NodeColor', orange)
% IDX----------------------------------------------------------------------
figure()

H=plot(tree_idx,'EdgeAlpha',0.7,'EdgeColor', blue,'LineStyle','-','LineWidth',1.2,...
    'MarkerSize',8, 'NodeColor', blue);
nicePlotting('IDX','','$t$');


ax = gca;
ax.XColor = 'white';
set(gca,'TickLabelInterpreter', 'latex');
set(gca,'xtick',[]);
set(gca,'ytick',[]);

ax.YGrid= 'on';

highlight(H,[4],'NodeColor', orange);
% NB_----------------------------------------------------------------------
figure()
H = plot(tree_nb,'EdgeAlpha',0.7,'EdgeColor', blue,'LineStyle','-','LineWidth',1.2,...
    'MarkerSize',8, 'NodeColor', blue);
nicePlotting('D','','$t$');

ax = gca;
ax.XColor = 'white';
set(gca,'TickLabelInterpreter', 'latex');
set(gca,'xtick',[]);
set(gca,'ytick',[]);

ax.YGrid= 'on';
highlight(H,[4],'NodeColor', orange);