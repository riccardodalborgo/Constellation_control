function clusterPlot(file)
% clusterPlot  plot the the connected components of the final configuration
%              of the network
%
% Input:
%      - file: mat file produced by the simulation routine main_sim.m
%

load(file)

A = datasim.A;
nodes = datasim.nodes;
G = graph(A);
figure()

p = plot(G,'EdgeAlpha',0.7,'LineStyle','-','LineWidth',1.2,...
    'MarkerSize',8,'XData',nodes(:,1),'YData',nodes(:,2));
nicePlotting('','','');
colormap jet                               % select color palette 
bins= conncomp(G);                         % define edge colors
p.NodeCData= bins;
p.NodeLabel = {};

set(gca,'xtick',[], 'ytick',[]);
set(gca,'Visible','off');

ylim([-0.05, 1.05]);
xlim([-0.05, 1.05]);

% Compute color  steps
cmap = jet(256);
step = 256/max(bins);

for i=1:length(G.Edges.EndNodes)
     colorRow = ceil(bins(G.Edges.EndNodes(i,1))*step);
     highlight(p,G.Edges.EndNodes(i,:),'EdgeColor',cmap(colorRow, :))
end

end