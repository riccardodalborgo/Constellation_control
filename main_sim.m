%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   SIMULATION ROUTINE                                                    %                  %
%   This routine compute the simulation of a cubesat network.             %
%   Both the alignment phase & the conflict resolution are handled and    %
%   at the end of the routine all data are saved in the folder:           %
%       ./simulation_results                                              %
%                                                                         %
%   In the section "User defined params" there are some tweakable input   %
%   parameters :                                                          %
%       - cluster  :   mark if the simulation is run on the cluster and so% 
%                      the results are not plotted at the end             %       
%       - filename :   mat file containing the graph                      %
%       - nsim     :   number of consecutive simulations run on the loaded%
%                      graph                                              %
%       - params   :   parameter vector with the following fields:        %
%                       -alpha_gain : stepsize of the gradient descent    %
%                       -IMU_noiseVar : variance of the gaussian noise    %
%                                       applied on the IMU sensor         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all; clear all; clc;

addpath ('./data');
addpath ('./tools');
addpath ('./tools/animation');
addpath ('./tools/conflicts');
addpath ('./tools/ctrl_design');
addpath ('./tools/SNR');
addpath ('./models');


%% User defined params

cluster = 0;                          %if cluster=1 do not plot the results
filename = 'graph5.mat';              % graph
nsim = 2;                             % number of simulation to be performed

% Params first simulation
params(1).alpha_gain=10;
params(1).IMU_noiseVar= 3.4e-7;                %0.1 deg

% Params second simulation
params(2).alpha_gain=10;
params(2).IMU_noiseVar= 3.4e-7;                %0.1 deg
%-------------------------------------------------------------------------%
% NOTE: For more simulation stack more tuple in params(i) vector          %
%-------------------------------------------------------------------------%
%% Starting nsim simulations
for sim_i = 1:nsim
    loadParameters(filename,params(sim_i));
    simulate
    datasim = simulation_data;
    datasim.params = params(sim_i);
    datasim.cubesat_type = cubesat_type;
    
    %Save the results
    newStr = split(filename,'.');
    file{sim_i} = char(strcat(newStr(1), '_', num2str(sim_i),'.mat'));
    
    save(strcat('./simulation_results/',file{sim_i}),'datasim');
    
    
end

%% Plot the results 

if ~cluster
    for sim_i = 1:nsim
        sprintf('Display Simulation %i results', sim_i)
        plotData(file{sim_i})
    end
end