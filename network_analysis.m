%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  NETWORK ANALYSIS ROUTINE                                               %
%   This routine performs the analysis of the network obtained at the end %
%   of the alignment simulation. Different metrics are considered:        %
%       - SNR : based on link quality                                     %
%       - IDX : based on a trade-off between link quality & connectivity  %
%       - NB  : based on connectivity                                     %
%                                                                         %
%   The value of the variable 'pth' is the path of the target folder where%
%   are all data to be computed. The routine start from the output of the %
%   main_sim.m routine and computes the network performances              %
%                                                                         % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all; clear all;

addpath ('./data');
addpath ('./tools');
addpath ('./tools/animation');
addpath ('./tools/bellmanFord');
addpath ('./tools/conflicts');
addpath ('./tools/ctrl_design');
addpath ('./tools/network');
addpath ('./tools/SNR');
addpath ('./tools/graphs');
addpath ('./models');
addpath ('./simulation_results');
%% Load all data in the desired Folder

pth = 'simulation_results/';
% Scan the folder
files = dir(pth);

% Load all .mat files
Z=1;
for K = 1 : length(files)
    if ~files(K).isdir && ~strcmp(files(K).name, '.gitignore')
        load(strcat(pth,files(K).name))
        data(Z) = datasim;
        Z=Z+1;
    end
end

for K=1:size(data,2)   
    %% Choose data
    datasim = data(K);
    
    G = digraph(datasim.A);
    G_undirected = graph(datasim.A);
    nodes = datasim.nodes; 
    idx = datasim.idx;
    snr_map = datasim.snr_map;

    D = outdegree(G);
    n = height(G.Nodes);
    m = height(G.Edges);

    %% Plot network
    fig = figure('Name','Network after alignment phase');
    p = plot(G_undirected,'XData',nodes(:,1),'YData',nodes(:,2));
    G.Nodes.ID = cellfun(@str2num, p.NodeLabel');
    G.Nodes.NodeOutDegree = D;
    p.NodeCData = G.Nodes.NodeOutDegree;
    c = colorbar;

    c.Label.String = 'Node outdegree';
    nicePlotting('Aligned Network', '', '');
    ylim([-0.05, 1.05]);
    xlim([-0.05, 1.05]);

    %% Network weighted by neighbor degree
    G_nb = G;

    % Adding weigths as 1/# of neighbours 
    for i = 1:m
          G_nb.Edges.Weight(i) = 1/G.Nodes.NodeOutDegree(G.Edges.EndNodes(i,2));
    end  

    % Extraction of main connected component (MCC)
    G_nb = getMCC(G_nb);
    n_nb = height(G_nb.Nodes);

    %% Network weighted by link snr
    G_snr = G;

    % Adding weigths as 1/snr(Rx)
    for i = 1:m
        rx = G_snr.Edges.EndNodes(i,2);
        tx = G_snr.Edges.EndNodes(i,1);
        snr = sum(sum(snr_map(tx,rx).SNR));   % using the fact only one differs form 0
        G_snr.Edges.Weight(i) = 1/snr;
    end

    % Extraction of main connected component (MCC)
    G_snr = getMCC(G_snr);
    n_snr = height(G_snr.Nodes);

    %% Network weighted by index
    G_idx = G;
    % Adding weigths as 1/idx 
    for i = 1:m
        rx = G_idx.Edges.EndNodes(i,2);
        G_idx.Edges.Weight(i) = 1/idx(rx);   
    end  

    % Extraction of main connected component (MCC)
    G_idx = getMCC(G_idx);
    n_idx = height(G_idx.Nodes);

    %---------------------------------------------------------------------%
    %           STARTING NETWORK EVALUATION                               %
    %                                                                     %
    %---------------------------------------------------------------------%
    %% Compute communications tree for nb weight
    for root=1:n_nb
        %Compute tree
        tree_nb = BellmanFord(G_nb,root);

        %Populate the forest
        forest_nb(root,1)  = {tree_nb};

    end

    %Evaluate performance
         performance_nb = getNetworkPerformance(forest_nb,snr_map);

    %% Compute communications tree for snr weight
    for root=1:n_snr
        %Compute tree
        tree_snr = BellmanFord(G_snr,root);

        %Populate the forest
        forest_snr(root,1)  = {tree_snr};
    end

    %Evaluate performance
        performance_snr = getNetworkPerformance(forest_snr, snr_map);

    %% Compute communications tree for idx weight
    for root=1:n_snr
        %Compute tree
        tree_idx = BellmanFord(G_idx,root);

        %Populate the forest
        forest_idx(root,1)  = {tree_idx};
    end

    %Evaluate performance
        performance_idx = getNetworkPerformance(forest_idx,snr_map);

    %% Save the data
    path= strcat('./',pth,'network/',files(K+3).name);

    performance.n_nodes= n;
    performance.MCC=n_nb;
    performance.nb=performance_nb;
    performance.snr=performance_snr;
    performance.idx=performance_idx;

    % Print results
    disp(performance);
  
    % % Save
    save(path , 'performance');
    clearvars -except data files pth
end

